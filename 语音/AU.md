## 德语语音学习

### A a U u 元音学习

**长短音规则**  
  
|ID|规则|例子|    
|------|------|------|  
|1|两个相同元音叠加在一起，该元音发长音|Maat, Paar, Haar|  
|2|元音的后面有字母h,该元音发长音，字母h不发音| nahmen, Hahn, Lahm, Kahn|  
|3|单词中，在元音的后面只有1个或者没有辅音字母，该元音发长音(后缀除外)|Papa, Name, Laden(en是后缀)|  
|4|单词在元音后面有两个或者两个以上辅音，该元音发短音|Mann, Lampe, danke, Gast|  


**A a**  
**长音**  
  
|Wort|Bedeutung|
|----|----|
|Tag|白天|
|Papa|爸爸|
|Name|名字|
|Laden|商店|

**短音**  
  
|Wort|Bedeutung|
|----|---------|
|Mann|男人|
|Gast|客人|
|Lampe|灯|
|danke|谢谢|

**U u**  

**长音**  
  
|Wort|Bedeutung|
|----|---------|
|Kuh| 母牛|
|du|你|
|gut|好的，很好，不错|

**短音**   
  
|Wort|Bedeutung|
|---|---|
|Mutter|母亲|
|Lust|兴趣|
|Hamburg|汉堡(市)|

```
Guten Tag ! 你好(整个白天都可用)
```
