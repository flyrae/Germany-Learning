## St Sp

### 辅音 st 
/ʃt/  
三种情况  
1. /ʃt/ 出现在单词开头，后紧跟元音字母，t发爆破音
2. /ʃt/ 出现在单词开头，后紧跟辅音，不需要发爆破音,后接的辅音字母只能为r  
3. /st/ 不再开头 /st/
   
- /ʃt/ t发爆破音
    
  |Wort|Bedeutung|
  |---|---|
  |Stadt|城市(dt发/t/)|
  |stier|金牛座|
  |stehen|站立|
  |stau|交通阻塞|
  |steuer|税收|
  |student|(男)大学生|
  |studentin|(女)大学生|
  |stein|石头|

- /ʃt/ t不发爆破音  

  |Wort|Bedeutung|
  |---|---|
  |streiken|罢工|
  |straße|马路,街道<br>Nanjing-Strße 南京路|
  |strauß|花束，施特劳斯|
  |strom|电流|

- 不在开头 /st/
    
  |Wort|Bedeutung|
  |---|---|
  |fenster|窗户|
  |schwester|姐妹|
  |faust|浮士德|
   Du bist ... 你是...

在复合词中，st虽位于中间，仍是所属单词的首位 参照1,2  
  Einstein  
字母有前缀，参照1,2  
  verstehen 理解  
  ver: 前缀


### sp /ʃp/
 发音规则同st 1，2。 sp只存在于单词开头  
1. /ʃp/ 出现在单词开头，后紧跟元音字母，p发爆破音
2. /ʃp/ 出现在单词开头，后紧跟辅音，不需要发爆破音,后接的辅音字母只能为r  
   
- 1.发爆破音
    
  |Wort|Bedeutung|
  |---|---|
  |spät|迟的，晚的|
  |spiel|比赛，游戏|
  |sport|体育，运动|
  |spaß|乐趣，乐子|
  |spinnen|纺织,织(网);胡扯，胡说八道<br>Spinnst du? 你疯了吗?你秀逗了吗?|
  |speise|菜肴|

- 2.不发爆破音
    
  |Wort|Bedeutung|
  |---|---|
  |springen|跳跃|
  |sprung|跳跃(名词)|
  |sprechen|说话|

  Ich spreche Deutsch. 
  我说德语

```
    Bis spater!
    一会儿见!

    Bis morgen !
    明天见!

    morgen: 明天
```
