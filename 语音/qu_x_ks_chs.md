## qu x ks chs
### qu 
/kv/  
先发/k/,后发/v/,送气相对较重，带出随后的元音，后接r,e,u居多  
  
|Wort|Bedeutung|
|---|---|
|Qual|痛苦|
|Qualität|质量|
|Quelle|源泉，来源|
|Quittung|票据，发票|
|Quatsch|废话，胡说八道|
|Quadrat|平方<br>Meter 米 → Quadratmeter 平方米<br> kilometer 公里，千米 → Quadratkilometer 平方公里|

### x ks chs
读作 /ks/
  
|Wort|Bedeutung|
|---|---|
|text|文章|
|Marx|马克思|
|Alex|亚历山大的简称(Alexander)|
|Dax|Dax指数|
|links|左边的|
|sechs|六<br>16: sechzehn ch|
|wachsen|成长|
|Achse|轴|
|fuchs|狐狸|

### 重音规律
1. 德语词重音在首位，多在第一音节

   |Wort|Bedeutung|
   |---|---|
   |Abend|晚上，傍晚|
   |Abendessen|晚餐|
   |volkswagen|大众|
   |krankenhaus|医院|
   |Großvater|爷爷|
   |klassenzimmer|教室|

2. 外来词在末尾，一般在倒数第一第二音节上  
   **英语外来词**
     
   |Wort|Bedeutung|
   |---|---|
   |Computer|电脑|
   |Charakter|性格|
   |Hobby|兴趣|
   |Job|工作|

   **法语或西语外来词 酌情判断**

   |Wort|Bedeutung|
   |---|---|
   |Restaurant|饭店|
   |Orange|橙子🍊|
   |Journalist|记者|
   |Garage|车库|


```
    Das ist toll!
    太棒了！太给力了！

    toll 口语中常用的称赞词

    Wunderbar! 太棒了！太精彩了！
    Super! 超级好！
    Prima! (拉丁语) Das ist prima! 太棒了！太赞了！
    Fantastisch! 太棒啦！美爆了！
    Ausgezeichnet! 太棒了
``` 