### E L 

***E e***  
**长音**  
  
|Wort|Bedeutung|
|----|----|
|Tee|茶|
|kaffee|咖啡|
|Beethoven|贝多芬|
|Lehrer|老师(er,后缀不能卷舌)|

  
**短音**  
  
|Wort|Bedeutung|
|---|---|
|Bett|床|
|nett|可爱的，很可爱 (Du bist nett! 你好可爱呀!)|
|Berg|山,山峰 (Tai-Berg / TaiShan-Berg 泰山)
  
**弱音**  
  
1. 当字母e出现在单词的结尾的时候，需要弱读  
  
|Wort|Bedeutung|
|---|---|
|Tasse|杯子，咖啡杯|
|Lampe|灯|
|Name|名字 (Mein Name ist ...  我的名字是)|
  
2. 部分前缀和后缀中的字母e，需要弱读，例如前缀 ge, 后缀： en,er,el
  
|Wort|Bedeutung|
|---|---|
|gekommen|kommen(英：come)的过去分词形式|
|Lehrer|老师,er是后缀|
|leben|生活，居住(第一个长音，第二个弱音)|
|Tempel|寺庙|
||Beethoven|贝多芬|


***I i***   
**长音**  
  
|Wort|Bedeutung|
|---|---|
|Tiger|老虎|
|China|中国|
|lieben|爱 (不存在“ii”,q取而代之的是"ie",同样发长音)|
  
**短音**  
  
|wort|Bedeutung|
|---|---|
|dick|胖的( Du bist dick. 你好胖)|
|Adidas|阿迪达斯|

```
   Ich liebe dich.  (I love you.)
   德语中，主语如果是ich, 动词一律以e结尾，且需要弱读.（lieben 去掉n）


   Wie geht es Ihnen?  (How are you？)
   Danke, gut. (Fine, Thank you!)
```