### 字母组合 sch tsch ng
  
### 辅音 sch /ʃ/  
广泛用于德语人名  
  
|Wort|Bedeutung|
|---|---|
|Schneider|施耐德(姓);裁缝|
|Schmidt|施密特|
|Schwein|猪|
|Schau|看<br>Schau mal! 看呐!看!|
|Geschichte|故事,历史|
|Schatz|宝贝、亲爱的<br>Mein Schatz|
|schnee|雪|
|fleisch|肉|
|schön|漂亮<br>Du bist schön. 你真漂亮|
|schütze|射手座|
|fisch|鱼<br>[复]fische 鱼;双鱼座|

### 辅音 tsch /tʃ/
  
|Wort|Bedeutung|
|---|---|
|Deutsch|德语|
|Deutschland|德国、德意志|
|tschechisch|捷克的|
|Tschüss|再见|

再见
Auf Wiedersehen!
Tschüss!
Ciao!

### 辅音 ng
发鼻音  
  
|Wort|Bedeutung|
|---|---|
|Englisch|英语|
|lang|长的|
|jung|年轻的|
|junge|男孩|
|Übung|练习|
|Einladung|邀请|

ung: 常用于名词后缀中   

```
    Ich lerne Deutsch.
    lernen 学习

    Ich habe schwein!
    我真走运! 我走运啦!
    schwein 猪
```

