## S W
### S  
两种情况，发/z/ 或者 /s/  
**1 /z/**  
位于元音前， S 发/z/ 
  
|Wort|Bedeutung|
|---|---|
|Sonne|太阳|
|Dose|罐子，罐头|
|sehr|非常<br>sehr gut 非常好<br>sehr nett 非常可爱(Du bist sehr nett)<br>sehr klug 非常聪明|
|Sie|您<br> Sie sind... 您是...<br>Sie sind herr wang. 您是王先生.<br>Sind Sie herr Wang? 您是王先生?|
|sagen|说，说话<br>Ich sage.. 我说...|
|so|这样、如此<br>so gut! == so good<br>Ach so.原来如此。|
(Ach语气词，“哦，啊”之类，表示惊讶。ch 发 /h/)  

**2 /s/**  
2.1 在元音后，字母s没有紧跟元音 /s/  
  
|Wort|Bedeutung|
|---|---|
|Gas|气体|
|Lust|兴趣、爱好|
|fast|几乎|
|ist|是<br>Mein Name ist... <br> Du bist ...|

2.2 ss发/s/不管前后有无元音，字母ß/esstset/ 在单词中相当于ss 发/s/  
   
|Wort|Bedeutung|
|---|---|
|groß|大的<br>r : 德文字母<br>China ist sehr groß. 中国很大<br>Yao Ming ist sehr groß. 姚明很高|
|küssen|亲吻，吻<br>Ich küsse dich.|

### W
感觉像是/v/ 发音时上齿和下唇轻微接触，构成缝隙，气流通过缝隙，摩擦成音，同时振动声带.    
  
|Wort|Bedeutung|
|---|---|
|was|什么<br>Was ist das? 这是什么?|
|wassermann|水瓶座|
|widder|白羊座|
|waage|平秤座|
|wein|酒|
|wurst|香肠|

```
    Auf Wiedersehen! 再见!  
    wieder:重复，又
    sehen:看见，又
```