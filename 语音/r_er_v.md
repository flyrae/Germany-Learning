## 辅音 r er v 

### R r
小舌音  
    
|Wort|Bedeutung|
|---|---|
|rot|红色<br> Das ist rot.|
|drei|三|
|krebs|巨蟹座|
|fahrrad|自行车|
|Freiburg|弗莱堡|
|Frankfurk|法兰克福|

### 字母组合 er
两种情况  
1. er位于单词中间，字母e发短音,字母r弱读
     
   |Wort|Bedeutung|
   |---|---|
   |Bern|伯尔尼|
   |gern|喜欢|
   |berg|山|
   |lernen|学习|
2. 位于单词结尾时，多为名词后缀，字母e,r弱读  
     
    |Wort|Bedeutung|
    |---|---|
    |Lenrer|老师|
    |Vater|父亲|
    |Mutter|母亲|
    |Bauer|农民|
  
### V v
两种发音情况  
- 在德语本民族语言中读清辅音 /f/
- 在外来语中 /v/

**/f/**
  
|Wort|Bedeutung|
|---|---|
|Vater|父亲|
|vier|四|
|voll|满的，拥挤的|
|vieh|牲畜,牲口|
|folkswagen|大众汽车|

**/v/**
  
|Wort|Bedeutung|
|---|---|
|vitamin|维生素|
|vase|花瓶|
|visum|签证<br>[复] visa|
|villa|别墅|

```
    Es freut mich.
    很高兴认识您.

    freu 高兴
    mich 我(宾格)

    Es freut mich, Sie kenenenzulernen.
```