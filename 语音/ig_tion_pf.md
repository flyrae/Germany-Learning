## ig tion pf
  
### ig  
两种情况：  
1. 后缀/iʃ/
2. ig后面出现元音词尾 /ig/

- 1.后缀  
    形容词后缀 /iʃ/  
    
    |Wort|Bedeutung|
    |---|---|
    |wichtig|重要的|
    |richtig|正确的|
    |fähig|有能力的|
    |günstig|方便的|
    |mächtig|强有力的|
    |mutig|勇敢的|

    有些名词也以ig结尾
      
    |Wort|Bedeutung|
    |---|---|
    |Honig|蜂蜜|
    |Leipzig|莱比锡|
    |könig|国王|

    常会在ig结尾的形同词后加上 keit 构成名词，发音不变  
      
    |Wort|Bedeutung|
    |---|---|
    |wichtigkeit|重要性|
    |Fähigkeit|能力|

    “德国不少方言中 ig 读成/ik/”

- ig后面出现元音词尾(例如 en es er) /ig/
    
  |Wort|Bedeutung|
  |---|---|
  |wichtiger|更重要的|
  |günstiger|更方便的|
  |mächtiger|更厉害的|
  |tüchtiger|更能干的|


### tion
名词后缀 /tsion/  
先发/ts/,后发/ion/,重音在/ion/
  
|Wort|Bedeutung|
|---|---|
|Lektion|课<br>Lektion eins 第一课<br>Lektion zwei 第二课|
|Information|信息|
|Dekoration|装饰|
|station|车站|
|revolution|革命|
|opposition|反对派|

ssion,lion发音类似，辅音发音不同,ion发音一样
  
Diskussion 讨论  
Million 百万  

### pf 
轻发/p/ 重发/f/  

|Wort|Bedeutung|
|---|---|
|pferd|马|
|pfund|磅(453克(๑•̀ㅂ•́)و✧)，英镑|
|Apfel|苹果🍎|
|Topf|锅子|
|opfer|牺牲者|
|pflegen|照顾，护理，保养(汽车、皮肤、头发)|
|pfui|呸，不要脸|
|pfeffer|胡椒|

```
    Entschuldigung! 对不起！
    Ent-schuldig-ung  or En-tschuldig-ung

    Tut mur Leid!
    我很遗憾！我很抱歉！

    Bitte! 没关系，不客气.
    Das macht nichts. 不要紧，不客气。
```