## 元音 ai/ei eu/äu

### ai/ei  
/ai/
类似 "爱"  
少数情况下 ay/ey 也发这个音
  
|Wort|Bedeutung|
|---|---|
|Mais|玉米|
|Kaiser|皇帝|
|Geige|小提琴|
|Bayern|巴伐利亚 <br> Bayern München 拜仁慕尼黑|
|Zeit|时间 <br>Ich habe Zeit. 我有时间|
|vorbei|结束了，过去了<br> 过去了就让他过去吧|
|mein|我的<br>Mein name ist...|
  
### eu/äu
发 /ɔi/  
类似于 boy 中的oy  
/ɔ/发得重一些，/i/轻一些  
  
|Wort|Bedeutung|
|---|---|
|heute|今天|
|feuer|火|
|gebäude|建筑物(ge和de中的e弱读)|
|leute|人们|
|Häuser|[复]房子<br>单：Haus<br>äu通常出现再单词的附属及某些动词里|
|Neun| 九|
|teuer|昂贵的<br> Das ist sehr teuer.这个很贵|


```
    Wie heißen Sie?
    您叫什么名字？
    Wie : 如何? 怎么样?
    Sie: 您
    heißen:叫。。。名字

    Wei heißen bitten?
    请问您叫什么名字？
    
    Ich heißen... 
    我的名字是...  我叫...
    Mein Name ist...
    我的名字是...
```

