## J j ch

### 辅音 J j /j/  
  
|Wort|Bedeutung|
|---|---|
|ja|是的，对的<br>(nein 不是，错的)|
|jacke|夹克衫|
|Juni|六月|
|Jesus|耶稣|
|Junge|男孩|
|jungfrau|处女座,处女|
|jung|年轻的<br>du bist jung|
|Juden|犹太人|

### ch
三种情况:
1. 类似于/ξ/，在 xi 和 hi之间
2. 在 a,o,u,au 后面时发/h/，类似于喝
3. 外来词里，出现在英语外来词里/k/<br>法语外来词发/s/<br>西语外来词/tʃ/
   
- 1. /ξ/
    
  |Wort|Bedeutung|
  |---|---|
  |ich|我(主格)|
  |mich|我(宾格)|
  |dich|你(宾格)<br>Ich liebe dich.|
  |schlecht|坏的，糟糕的|
  |küche|厨房|
  |nicht|否定词，不<br>du bist nicht jung.你不年轻啦|
  |China|中国|
  |Chemie|化学|
  |liecht|简单的，轻易的<br>Deutsch ist leicht.|

- 2. /h/
    
  |Wort|Bedeutung|
  |---|---|
  |acht|数词|
  |auch|也|
  |koch|厨师|
  |lauch|大葱|
  |lauchen|笑|
  |knochen|骨头|
  |Schuihmacher|舒马赫|

- 3. 外来词
  **英语外来词 /k/**   
    
  |Wort|Bedeutung|
  |---|---|
  |charakter|性格|
  |christ|基督|
  |chaos|混乱|
  |christine|克里斯蒂娜|
  |christian|克里斯蒂安|

  **法语外来词 /s/**  
  chance 机会
  
```
    Guten Tag! 你好(白天)
    Guten Morgen! 早上好!
    Guten Abend! 晚上好!
    Gute Nacht! 晚安!

    Alles Gute! 祝你一切顺利!
    alles 所有的事情，一切
```

