## z ts tz ds y

### 辅音 Z z
/ts/  
类似于"次", 但要轻读  
  
|Wort|Bedeutung|
|---|---|
|zug|火车|
|zoo|动物园|
|zehn|十|
|zeit|时间|
|zwilling|双胞胎;双子座|
|zauber|魔法|
|holz|木头|
|salzburg|萨尔茨堡|
|bezahlen|付账,买单<br> Bezahlen bitte! 请买单|

  
### 字母组合 ts tz ds
/ts/  
出现几率较小
  
|Wort|Bedeutung|
|---|---|
|Katze|猫|
|Platz|座位，广场|
|abends|在晚上|
|landsmann|同胞，老乡|

  
### 半元音 Y y
既有辅音发音，又有元音发音  

- 辅音 /j/ 
  少见
    
  |Wort|Bedeutung|
  |---|---|
  |Yoga|瑜伽|
  |Yoghurt|酸奶|
  |Yuan|元|
  
- 元音 
  和字母ü发音相同
    
  |Wort|Bedeutung|
  |---|---|
  |Physik|物理|
  |system|系统，体系|
  |Typ|类型，型号(可指心仪对象)<br>Er/Sie ist mein Typ. 他/她是我的菜.<br> Er/Sie ist nicht mein Typ.他/她不是...|
  |Gymnasium|文理中学|

```
    Nehmen Sie Platz Bitte! (命令句)
    请坐!

    nehmen 拿(动词)
    platz nehmen 坐下(固定搭配)

    Kommen Sie herein bitte!
    请进！

    herein 进来(方向副词)
    herin bitten! 请进！
    Herein! 进来!/进!
```