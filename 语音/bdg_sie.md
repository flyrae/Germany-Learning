## 辅音  
辅音分为清辅音和浊辅音  

**b,d,g:这三个浊辅音字母，位于单词或音节的末尾，将发对应的清辅音, p, t ,k**
  
|浊辅音字母|清辅音|
|---|---|
|b|p|
|d|t|
|g|k|

**单词末尾**
  
|Wort|Bedeutung|
|---|---|
|Bat|浴室|
|Hamburg|汉堡|
|hund|狗🐶|
|Land|国家|
  
**音节末尾**  
Obst 水果  
德语中一个元音就是一个音节，b在字母O后面，位于音节最后所以发p  

## 您 你

|Wort|Bedeutung|Verwendungszweck(Usage)|Notiz|
|---|---|---|---|
|Sie|您|德语一般用Sie,用于陌生人，点头之交...|S 发 /z/<br>S大写|
|du|你|用于关系特别好(好友，亲人，对小孩子，学生之间)<br>或者特别不好的人|du bist nett.   <br>du bist klug(smart).|
