## 元音 Ü au 和 辅音 L

### 元音 Ü ü

**长音**
  
|Wort|Bedeutung|
|---|---|
|Tür|门|
|Süden|南方|
|Tüte|纸袋|

**短音**  
  
|Wort|Bedeutung|
|---|---|
|fünt|五|
|Füller|钢笔|
|Müssen|必须|
|Müller|穆勒|
|München|慕尼黑|

### 元音 au
  
|Wort|Bedeutung|
|---|---|
|Auge|眼睛|
|Baum|树|
|Haus|房子|
|Donau|多瑙河|

### 辅音 L l

- 规则1： l后面紧跟元音时，发音和汉语英语一样  
    Laden 商店
- 规则2：没有紧跟元音时，发特殊的阻塞音(舌尖卷起，触碰口腔顶端，喉咙向外送气)
      
    |Wort|Bedeutung|
    |---|---|
    |Fußball|足球|
    |kalt|寒冷的|
    |Bild|图片|
    |Halt|站住，不要动|
    |Hilfe|救命|

```
    Ich komme aus ...
    我来自于...
    Ich komme aus China.
    Ich komme aus Shanghai.
    Beijing = Peking
    Qingdao = Tsingtao
```
